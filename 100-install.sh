#!/bin/bash

sudo apt install -y ubuntu-restricted-extras
sudo apt install chromium -y
sudo apt install vim -y
sudo apt install neovim -y
sudo apt install htop -y
sudo apt install bpytop -y
sudo apt install git -y
sudo apt install curl -y
sudo apt install wget -y
sudo ufw enable 

echo "#################################"
echo "######## software is done #######"
echo "#################################"
